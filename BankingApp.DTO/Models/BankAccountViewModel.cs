﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp.ViewModel.Models
{
    public class BankAccountViewModel
    {
        public int AccountId { get; set; }
        public double Balance { get; set; }
    }
}
