﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.EF.Enums;

namespace BankingApp.ViewModel.Models
{
    public class TransactionViewModel
    {
        public int TransactionId { get; set; }
        public double Amount { get; set; }
        public DateTime Date { get; set; }
        public string TransactionType { get; set; }
        public long UserAccountId { get; set; }
        public long? RecipientAccountId { get; set; }
    }
}
