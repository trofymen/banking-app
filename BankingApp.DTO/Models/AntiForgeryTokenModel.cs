﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BankingApp.ViewModel.Models
{
    public sealed class AntiForgeryTokenModel
    {
        [JsonProperty("formId")]
        public string formId { get; set; }
        [JsonProperty("antiForgeryToken")]
        public string antiForgeryToken { get; set; }
    }
}
