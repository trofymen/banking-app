﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.EF.Entities;

namespace BankingApp.ViewModel.Models
{
    public class UserInfoViewModel
    {
        public string UserName { get; set; }
        public BankAccountViewModel Account { get; set; }
    }
}
