﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp.ViewModel.Models
{
    public class TransferViewModel
    {
        [Required]
        [Display(Name = "RecipientAccount")]
        public int RecipientAccount { get; set; }

        [Required]
        [Display(Name = "MoneyToTransfer")]
        public double MoneyToTransfer { get; set; }
    }
}
