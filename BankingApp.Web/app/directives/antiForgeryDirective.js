﻿(function() {
    'use strict';
    function antiForgeryDirectiveController(antiForgeryService, $scope, appInterceptorService) {

        $scope.antiForgeryToken = '';
        $scope.id = '';

        $scope.activate = function () {
            $scope.currentForm.style.display = "none";
            var xsrfId = appInterceptorService.xsrfData.xsrfId;
            appInterceptorService.xsrfData.xsrfId = appInterceptorService.xsrfData.xsrfToken = '';
            antiForgeryService.getAntiForgeryToken(xsrfId).then(function (data) {
                $scope.currentForm.style.display = "block";
                $scope.antiForgeryToken = data.antiForgeryToken;
                $scope.id = data.formId;
                appInterceptorService.addUpdateAntiForgeryTokenEvent($scope.activate, $scope.id);
            });
        };
    }

    function antiForgeryTokenDirective() {
        return {
            scope: {},
            controllerAs: 'directive',
            template: '<input id=attr name="antiForgeryTokenFormId" type="hidden" value="{{id}}" /> ' +
                '<input name="antiForgeryToken" type="hidden" value="{{antiForgeryToken}}" />',
            link: function ($scope, element, attrs) {
                $scope.currentForm = element.parent()[0];
                $scope.activate();
            },
            controller: ['antiForgeryService', '$scope', 'appInterceptorService', antiForgeryDirectiveController]
        }
    }

    angular.module("AngularAuthApp").directive('antiforgerytoken', antiForgeryTokenDirective);
})();