﻿var app = angular.module('AngularAuthApp',
['ngRoute', 'LocalStorageModule', 'angular-loading-bar']);

app.config(function($routeProvider, $locationProvider) {

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: true
    });

    $routeProvider.when('/', {
        controller: "loginController",
        templateUrl: "/app/views/login.html"
    });

    $routeProvider.when("/home", {
//deposit page
        controller: "homeController",
        templateUrl: "/app/views/home.html"
    });

    $routeProvider.when("/login", {
        controller: "loginController",
        templateUrl: "/app/views/login.html"
    });

    $routeProvider.when("/signup", {
        controller: "signupController",
        templateUrl: "/app/views/signup.html"
    });

    $routeProvider.when("/withdraw", {
        controller: "withdrawController",
        templateUrl: "/app/views/withdraw.html"
    });

    $routeProvider.when("/transfer", {
        controller: "transferController",
        templateUrl: "/app/views/transfer.html"
    });

    $routeProvider.when("/transactions", {
        controller: "transactionsController",
        templateUrl: "/app/views/transactions.html",
    });

    $routeProvider.otherwise({
        redirectTo: "/"
    });
}).constant('apiUrl', 'http://localhost:7253/');

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('appInterceptorService');
});

app.run(['authService', function (authService) {
    authService.fillAuthData();
}]);

(function () {
    function bootstrapApplication() {
        angular.element(document).ready(function () {
            angular.bootstrap(document, ["AngularAuthApp"]);
        });
    }
    bootstrapApplication();
}());