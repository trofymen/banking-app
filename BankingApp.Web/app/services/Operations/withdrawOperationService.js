﻿'use strict';
app.factory('withdrawOperationService', ['$http', function ($http) {

    var serviceBase = 'http://localhost:7253/';
    var withdrawOperationServiceFactory = {};

    var _withdrawOperation = function (withdrawData) {
        return $http.post(serviceBase + 'api/operation/withdraw', withdrawData, {
            headers:
           { 'Content-Type': 'application/json' }
        }).then(function (results) {
            return results;
        })
    }

    withdrawOperationServiceFactory.withdrawOperation = _withdrawOperation;

    return withdrawOperationServiceFactory;
}]);