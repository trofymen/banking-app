﻿'use strict';
app.factory('depositOperationService', ['$http', function ($http) {

    var serviceBase = 'http://localhost:7253/';
    var depositOperationServiceFactory = {};

    var _depositOperation = function (depositData) {
        return $http.post(serviceBase + 'api/operation/deposit', depositData, {
            headers:
            { 'Content-Type': 'application/json' }
        }).then(function(results) {
            return results;
        });
    }

    depositOperationServiceFactory.depositOperation = _depositOperation;

    return depositOperationServiceFactory;
}]);