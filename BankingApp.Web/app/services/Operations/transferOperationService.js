﻿'use strict';
app.factory('transferOperationService', ['$http', function ($http) {

    var serviceBase = 'http://localhost:7253/';
    var transferOperationServiceFactory = {};

    var _transferOperation = function (transferData) {
        return $http.post(serviceBase + 'api/operation/transfer', transferData, {
            headers:
            { 'Content-Type': 'application/json' }
        }).then(function(results) {
            return results;
        });
    }

    var _getRecipientAccounts = function () {
        return $http.get(serviceBase + 'api/operation/allaccounts', {
            headers:
           { 'Content-Type': 'application/json' }
        }).then(function (results) {
            return results;
        })
    }

    transferOperationServiceFactory.transferOperation = _transferOperation;
    transferOperationServiceFactory.getRecipientAccounts = _getRecipientAccounts;

    return transferOperationServiceFactory;
}]);