﻿'use strict';
app.factory('transactionsService', ['$http', function ($http) {

    var serviceBase = 'http://localhost:7253/';
    var transactionsServiceFactory = {};

    var _getTransactions = function () {
        return $http.get(serviceBase + 'api/operation/transactions', {
            headers:
            { 'Content-Type': 'application/json' }
        }).then(function (results) {
            return results;
        });
    }

    transactionsServiceFactory.getTransactions = _getTransactions;

    return transactionsServiceFactory;
}]);