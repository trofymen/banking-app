﻿'use strict';
app.factory('antiForgeryService', ['apiUrl', '$http', '$q', 'appInterceptorService', function (apiUrl, $http, $q, appInterceptorService) {

    var antiForgeryServiceFactory = {};

    var getDataFromHiddenInputs = function (formName) {
        var forms = document.getElementsByName(formName);
        if (forms) {
            var form = forms[0];
            if (form) {
                for (var i = 0; i < form.length; i++) {
                    if (form[i].name === 'antiForgeryTokenFormId') {
                        appInterceptorService.xsrfData.xsrfId = form[i].value;
                    }
                    else if (form[i].name === 'antiForgeryToken') {
                        appInterceptorService.xsrfData.xsrfToken = form[i].value;
                    }
                }
            }
        }
    }

    var getAntiForgeryToken = function (xsrfId) {
        var deferred = $q.defer();

        $http.get(apiUrl + 'api/antiforgerytoken?formId=' + xsrfId).success(function (data) {
            deferred.resolve(data);
        });

        return deferred.promise;
    };

    antiForgeryServiceFactory.getAntiForgeryToken = getAntiForgeryToken;
    antiForgeryServiceFactory.getDataFromHiddenInputs = getDataFromHiddenInputs;

    return antiForgeryServiceFactory;
}]);