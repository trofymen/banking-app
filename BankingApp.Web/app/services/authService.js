﻿'use strict';
app.factory('authService', ['$http', '$q', 'localStorageService', 'userInfoService', function ($http, $q, localStorageService, userInfoService) {

    var serviceBase = 'http://localhost:7253/';
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: "",
        balance: "",
        accountId: ""
    };

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
            return response;
        });
    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" +
        loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();

        $http.post(serviceBase + '/oauth/token', data, {
            headers:
            { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function (response) {

            localStorageService.set('authorizationData',
            { token: response.access_token, userName: loginData.userName });

            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;
    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.userName = '';
    };

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
            _userInfo();
        }
        else {
            _logOut();
        }
    }

    var _userInfo = function () {       //(when f5)
        userInfoService.getUserInfo().then(function (response) {
            if (response.data.status == 0) {
                _authentication.balance = (response.data.data.account.balance === 0) ? '0.00' : response.data.data.account.balance;
                _authentication.accountId = response.data.data.account.accountId;
            }
            else {
                _logOut();
            }
        });
    };

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.userInfo = _userInfo;

    return authServiceFactory;
}]);