﻿'use strict';
app.factory('userInfoService', ['$http','localStorageService', function ($http, localStorageService) {

    var serviceBase = 'http://localhost:7253/';
    var userInfoServiceFactory = {};

    var _getUserInfo = function () {
        return $http.get(serviceBase + 'api/clientinfo').then(function(results) {
            return results;
        });
    }   

    userInfoServiceFactory.getUserInfo = _getUserInfo;

    return userInfoServiceFactory;
}]);