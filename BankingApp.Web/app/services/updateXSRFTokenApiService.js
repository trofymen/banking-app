﻿'use strict';
app.factory('updateXSRFTokenApi',[ function () {
    var updateXSRFTokenApiServiceFactory = {};

    var event = function() {};
    var idAntiForgeryInput = '';
    var xsrfData = {
        xsrfId : '',
        xsrfToken : ''
}

    var getDataFromHiddenInputs = function(formName){
        var data = [];
        var forms = document.getElementsByName(formName);
        if (forms) {
            var form = forms[0];
            if (form) {
                for (var i = 0; i < form.length; i++) {
                    if (form[i].name === 'antiForgeryToken') {
                        data.push(form[i].value);
                    }
                }
                if (data && data.length === 2) {
                    xsrfData.xsrfId = data[0];
                    xsrfData.xsrfToken = data[1];
                }
            }
        }
    }

    updateXSRFTokenApiServiceFactory.Event = event;
    updateXSRFTokenApiServiceFactory.idAntiForgeryInput = idAntiForgeryInput;
    updateXSRFTokenApiServiceFactory.xsrfData = xsrfData;
    updateXSRFTokenApiServiceFactory.getDataFromHiddenInputs = getDataFromHiddenInputs;

    return updateXSRFTokenApiServiceFactory;
}]);