﻿'use strict';
app.factory('appInterceptorService', ['$q', '$location', 'localStorageService', function ($q, $location, localStorageService) {

    var appInterceptorServiceFactory = {};

    var xsrfData = {
        xsrfId: '',
        xsrfToken: ''
    }

    var events = [];

    var addUpdateAntiForgeryTokenEvent = function (func, tokenId) {
        events[tokenId] = func;
    };

    var updateAntiForgeryToken = function (tokenId) {
        events[tokenId]();
    };

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
            config.headers['XSRF-TOKEN'] = xsrfData.xsrfId + '!' + xsrfData.xsrfToken;
        }

        return config;
    }

    var _response = function (response) {
        if (response.status === 200 && xsrfData.xsrfId.length > 0)
            updateAntiForgeryToken(xsrfData.xsrfId);
        return response;
    }

    var _responseError = function (rejection) {
        if (rejection.status === 401) {
            localStorageService.remove('authorizationData');
            localStorageService.remove('antiForgeryCookie');
            $location.path('/login');
        }
        return $q.reject(rejection);
    }

    appInterceptorServiceFactory.request = _request;
    appInterceptorServiceFactory.response = _response;
    appInterceptorServiceFactory.responseError = _responseError;

    appInterceptorServiceFactory.xsrfData = xsrfData;
    appInterceptorServiceFactory.events = events;
    appInterceptorServiceFactory.updateAntiForgeryToken = updateAntiForgeryToken;
    appInterceptorServiceFactory.addUpdateAntiForgeryTokenEvent = addUpdateAntiForgeryTokenEvent;

    return appInterceptorServiceFactory;
}]);