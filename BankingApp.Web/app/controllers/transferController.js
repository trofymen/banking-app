﻿'use strict';
app.controller('transferController', ['$scope', '$timeout', 'authService', 'transferOperationService', 'antiForgeryService', function ($scope, $timeout,
    authService, transferOperationService, antiForgeryService) {

    $scope.dataLoading = false;

    $scope.message = '';
    $scope.transferData = '';

    $scope.regExpression = '^\[0-9]+$';
    $scope.isAccountSelected = false;

    $scope.accountsNumbers = [];

    $scope.transferInfo = {
        recipientAccount: "Accounts",
        moneyToTransfer: ""
    }

    $scope.savedSuccessfully = true;

    var getAccounts = function() {

        $scope.dataLoading = true;

        transferOperationService.getRecipientAccounts().then(function (response) {

            $scope.dataLoading = false;

            if (response.data.status == 0) {
                var info = response.data.data;
                if (info.length > 0) {
                    info.forEach(function (item, i, arr) {
                        $scope.accountsNumbers.push(item);
                    });
                }
            }
            else {
                $scope.savedSuccessfully = false;
                if (response.data.status == 2) {
                    $scope.message = response.data.message;
                }
                else {
                    $scope.message = 'error happened, Please try again';
                }
            }
        },
         function (err) {
             handleResponse();
             $scope.savedSuccessfully = false;
             if (err.data.message) {
                 $scope.message = err.data.message + ' Please try again!';
             }
             else {
                 $scope.message = 'Error! Please try again!';
             }
         });
    }

    $scope.dropBoxItemSelected = function (item) {
        $scope.transferInfo.recipientAccount = item;
        $scope.isAccountSelected = true;
    }

    $scope.transfer = function (formName) {

        $scope.dataLoading = true;
        antiForgeryService.getDataFromHiddenInputs(formName);
        $scope.transferInfo.moneyToTransfer = $scope.transferData;
        transferOperationService.transferOperation($scope.transferInfo).then(function (response) {
            handleResponse();
            if (response.data.status == 0) {
                authService.authentication.balance = response.data.data;
                $scope.message = 'completed successfully!';
            }
            else {
                $scope.savedSuccessfully = false;
                if (response.data.status == 2) {
                    $scope.message = response.data.message;
                }
                else {
                    $scope.message = 'error happened, Please try again';
                }
            }
        },
         function (err) {
             handleResponse();
             $scope.savedSuccessfully = false;
             if (err.data.message) {
                 $scope.message = err.data.message + ' Please try again!';
             }
             else {
                 $scope.message = 'Error! Please try again!';
             }
         });
    };

    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $scope.transferData = '';
            $scope.message = '';
            $scope.isAccountSelected = false;
            $scope.transferInfo.recipientAccount = 'Recipient accounts';
        }, 2000);
    }

    var handleResponse = function () {
        $scope.dataLoading = false;
        startTimer();
    }

    if (authService.authentication.isAuth) {
        getAccounts();
    }
}]);