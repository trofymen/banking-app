﻿'use strict';
app.controller('withdrawController', ['$scope', '$timeout', 'authService', 'withdrawOperationService', 'antiForgeryService', function ($scope, $timeout,
    authService, withdrawOperationService, antiForgeryService) {

    $scope.dataLoading = false;

    $scope.message = '';
    $scope.withdrawData = '';

    $scope.regExpression = '^\[0-9]+$';


    $scope.savedSuccessfully = true;

    $scope.withdraw = function (formName) {

        $scope.dataLoading = true;
        antiForgeryService.getDataFromHiddenInputs(formName);

        withdrawOperationService.withdrawOperation($scope.withdrawData).then(function (response) {

            handleResponse();

            if (response.data.status == 0) {
                authService.authentication.balance = response.data.data;
                $scope.message = 'completed successfully!';
            }
            else {
                $scope.savedSuccessfully = false;
                if (response.data.status == 2) {
                    $scope.message = response.data.message;
                }
                else {
                    $scope.message = 'error happened, Please try again';
                }
            }
        },
         function (err) {
             handleResponse();
             $scope.savedSuccessfully = false;
             $scope.message = err.data.message + ' Please try again!';
         });
    };

    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $scope.withdrawData = '';
            $scope.message = '';
        }, 2000);
    }

    var handleResponse = function () {
        $scope.dataLoading = false;
        startTimer();
    }
}]);