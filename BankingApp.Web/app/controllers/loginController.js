﻿'use strict';
app.controller('loginController', ['$scope', '$location', '$timeout', 'authService', 'localStorageService', 'userInfoService',
    function ($scope, $location, $timeout, authService, localStorageService, userInfoService) {

    $scope.loginData = {
        userName: "",
        password: ""
    };

    $scope.message = "";
    $scope.dataLoading = false;

    $scope.login = function () {

        $scope.dataLoading = true;

        authService.login($scope.loginData).then(function (response) {
            userInfo();
        },
         function (err) {
             handleResponse();
             if (err) {
                 $scope.message = err.error_description;
             }
             else {
                 $scope.message = 'Invalid request. Please try again!';
             }
         });
    };

    $scope.goSignUp = function() {
        $location.path('/signup');
    }

    var userInfo = function () {
        userInfoService.getUserInfo().then(function (response) {
            if (response.data.status == 0) {
                authService.authentication.balance = (response.data.data.account.balance === 0) ? '0.00' : response.data.data.account.balance;
                authService.authentication.accountId = response.data.data.account.accountId;

                $location.path('/home');
            }
            else if (response.data.status == 1) {
                $scope.message = 'error happened, Please try again';
            }
        },
         function (err) {
             $scope.message = err.data.message;
         });
    };

    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $scope.message = '';
        }, 2000);
    }

    var handleResponse = function () {
        $scope.dataLoading = false;
        startTimer();
    }


    var authData = localStorageService.get('authorizationData');
    if (authData) {
        $location.path('/home');
    }
    else {
        authService.logOut();
    }
}]);