﻿'use strict';
app.controller('transactionsController', ['$scope', '$timeout', 'authService', 'transactionsService', function ($scope, $timeout,
    authService, transactionsService) {

    $scope.dataLoading = false;

    $scope.message = '';

    $scope.savedSuccessfully = true;

   var transactionInfo = {
        transactionNumber: '',
        senderAccount: '',
        recipienAccount: '',
        amount: '',
        transactionType: '',
        date: ''
    }

    $scope.transactionsData = [];

    var transactions = function () {

        $scope.dataLoading = true;

        transactionsService.getTransactions().then(function (response) {

            handleResponse();

            if (response.data.status == 0) {
                var info = response.data.data;
                if (info.length > 0) {
                    info.forEach(function (item, i, arr) {
                        var transaction = {
                            transactionNumber: item.transactionId,
                            senderAccount: item.userAccountId,
                            recipienAccount: !item.recipientAccountId ? 'none' : item.recipientAccountId,
                            amount: item.amount,
                            transactionType: item.transactionType,
                            date: item.date
                        }
                        $scope.transactionsData.push(transaction);
                    });
                }
                $scope.message = 'completed successfully!';
            }
            else {
                $scope.savedSuccessfully = false;
                if (response.data.status == 2) {
                    $scope.message = response.data.message;
                }
                else {
                    $scope.message = 'error happened, Please try again';
                }
            }
        },
         function (err) {
             handleResponse();
             $scope.savedSuccessfully = false;
             $scope.message = err.data.message + ' Please try again!';
         });
    };

    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $scope.message = '';
        }, 2000);
    }

    var handleResponse = function () {
        $scope.dataLoading = false;
        startTimer();
    }

    if (authService.authentication.isAuth) {
        transactions();
    }
}]);