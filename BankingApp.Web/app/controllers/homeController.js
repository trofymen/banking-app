﻿'use strict';
app.controller('homeController', [
    '$scope', '$timeout', '$location', 'authService', 'depositOperationService', 'localStorageService',
    'updateXSRFTokenApi', 'antiForgeryService', function ($scope, $timeout, $location, authService, depositOperationService, localStorageService,
        updateXSRFTokenApi, antiForgeryService) {

        $scope.dataLoadingForm1 = false;

        $scope.messageForm = '';
        $scope.depositData = '';

        $scope.regExpression = '^\[0-9]+$';

        $scope.savedSuccessfully = true;

        $scope.deposit = function(formName) {
            var depositData = 0;
            $scope.dataLoading = true;
            antiForgeryService.getDataFromHiddenInputs(formName);
            depositData = $scope.depositData;

            depositOperationService.depositOperation(depositData).then(function(response) {
                    handleResponse();
                    if (response.data.status === 0) {
                        authService.authentication.balance = response.data.data;
                        $scope.messageForm = 'completed successfully!';
                        $scope.message = 'completed successfully!';
                    } else {
                        $scope.savedSuccessfully = false;
                        if (response.data.status === 2) {
                                $scope.messageForm = response.data.message;
                        } else {
                                $scope.messageForm = 'error happened, Please try again';
                        }
                    }
                },
                function(err) {
                    handleResponse();
                    $scope.savedSuccessfully = false;
                        $scope.messageForm = err.data.message + ' Please try again!';
                });
        };

    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $scope.depositData= '';
            $scope.messageForm = '';
        }, 2000);
    }

    var handleResponse = function () {
        $scope.dataLoading = false;
        startTimer();
    }

    var authData = localStorageService.get('authorizationData');
    if (!authData) {
        authService.logOut();
        $location.path('/login');
    }
}]);