﻿'use strict';
app.controller('signupController', ['$scope', '$location', 
'$timeout', 'authService', function ($scope, $location, $timeout, authService) {

    $scope.savedSuccessfully = false;
    $scope.message = "";

    $scope.registration = {
        userName: "",
        password: "",
        confirmPassword: ""
    };
    $scope.regExpression = '^([a-zA-ZА-Яа-я][a-zA-ZА-Яа-я0-9]*)$';
    $scope.dataLoading = false;

    $scope.signUp = function () {
        $scope.dataLoading = true;
        authService.saveRegistration($scope.registration).then(function (response) {
            $scope.dataLoading = false;
            $scope.savedSuccessfully = true;
            $scope.message = "Registration successfully and " +
            "you will be redicted to login page in 2 seconds.";
            startTimer();
        },
         function (response) {
             var errors = [];
             for (var key in response.data.modelState) {
                 for (var i = 0; i < response.data.modelState[key].length; i++) {
                     errors.push(response.data.modelState[key][i]);
                 }
             }
             $scope.message = "Failed: " + errors.join(' ');
             $scope.dataLoading = false;
         });
    };

    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $location.path('/login');
        }, 2000);
    }

}]);

//app.directive("compareTo", function () {
//    return {
//        require: "ngModel",
//        scope: {
//            otherModelValue: "=compareTo"
//        },
//        link: function (scope, element, attributes, ngModel) {

//            ngModel.$validators.compareTo = function (modelValue) {
//                return modelValue == scope.otherModelValue;
//            };

//            scope.$watch("otherModelValue", function () {
//                ngModel.$validate();
//            });
//        }
//    };
//});