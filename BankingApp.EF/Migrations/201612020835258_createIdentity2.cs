namespace BankingApp.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createIdentity2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AspNetRoles", newName: "Roles");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Roles", newName: "AspNetRoles");
        }
    }
}
