namespace BankingApp.EF.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Context;
    using Entities;
    using Enums;
    using global::EF.Entities;
    using global::EF.Entities.Identity;
    using Infrastructure;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<BankingApp.EF.Context.BankingAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(BankingAppContext context)
        {
            //This method will be called after migrating to the latest version.


            var userManager = new ApplicationUserManager(new ApplicationUserStore(new BankingAppContext()));
            var roleManager = new ApplicationRoleManager(new ApplicationRoleStore(new BankingAppContext()));

            if (roleManager.Roles.Count() == 0)
            {
                roleManager.Create(new Role { Name = RoleType.Admin.ToString() });
                roleManager.Create(new Role { Name = RoleType.Client.ToString() });
            }


            var users = new List<User>()
            {
                new User { UserName = "User1", Email = "1tai.jou@mymail.com", EmailConfirmed = true, CreateDate = DateTime.Now.AddYears(-3) },
                new User { UserName = "User2", Email = "2tai.jou@mymail.com", EmailConfirmed = true, CreateDate = DateTime.Now.AddYears(-1) },
                new User { UserName = "User3", Email = "3tai.jou@mymail.com", EmailConfirmed = true, CreateDate = DateTime.Now.AddYears(-2) }
            };

            userManager.Create(users[0], "test123");
            userManager.Create(users[1], "test123");
            userManager.Create(users[2], "test123");

            userManager.AddToRole(users[0].Id, RoleType.Admin.ToString());
            userManager.AddToRole(users[1].Id, RoleType.Client.ToString());
            userManager.AddToRole(users[2].Id, RoleType.Client.ToString());

            IList<Account> accounts = new List<Account>();

            accounts.Add(new Account() { UserId = users[0].Id, Balance = 100 });
            accounts.Add(new Account() { UserId = users[1].Id, Balance = 200 });
            accounts.Add(new Account() { UserId = users[2].Id, Balance = 300 });

            foreach (Account account in accounts)
                context.Accounts.Add(account);

            context.SaveChanges();
        }
    }
}
