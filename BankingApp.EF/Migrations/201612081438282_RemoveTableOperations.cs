namespace BankingApp.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveTableOperations : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Operations");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Operations",
                c => new
                    {
                        OperationId = c.Int(nullable: false, identity: true),
                        OperationName = c.String(),
                    })
                .PrimaryKey(t => t.OperationId);
            
        }
    }
}
