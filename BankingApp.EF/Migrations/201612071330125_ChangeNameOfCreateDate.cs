namespace BankingApp.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeNameOfCreateDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "CreateDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Users", "JoinDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "JoinDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Users", "CreateDate");
        }
    }
}
