namespace BankingApp.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Testmigration2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Login", c => c.String(nullable: false, maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Login", c => c.String(nullable: false, maxLength: 28));
        }
    }
}
