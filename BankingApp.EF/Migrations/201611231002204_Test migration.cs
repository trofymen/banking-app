namespace BankingApp.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Testmigration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Login", c => c.String(maxLength: 28, nullable: false));
            
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Login", c => c.String(maxLength: 30, nullable: false));
        }
    }
}
