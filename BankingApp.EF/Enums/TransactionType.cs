﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp.EF.Enums
{
    public enum TransactionType
    {
        Deposit,
        Withdraw,
        Transfer
    }
}
