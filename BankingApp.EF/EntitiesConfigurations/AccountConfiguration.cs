﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using BankingApp.EF.Entities;

namespace BankingApp.EF.EntitiesConfigurations
{
    class AccountConfiguration: EntityTypeConfiguration<Account>
    {
        public AccountConfiguration()
        {
            this.Property(p => p.Balance).IsRequired().IsConcurrencyToken();
            this.Property(p => p.RowVersion).IsRowVersion();
            this.HasRequired<User>(p => p.User).WithMany(p => p.Accounts);
        }
    }
}
