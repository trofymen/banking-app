﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using BankingApp.EF.Entities;

namespace BankingApp.EF.EntitiesConfigurations
{
    class TransactionConfiguration : EntityTypeConfiguration<Transaction>
    {
        public TransactionConfiguration()
        {
            this.Property(p => p.Date).IsRequired();
            this.Property(p => p.Amount).IsRequired();
            this.Property(p => p.TransactionType).IsRequired();
            this.HasRequired<Account>(p => p.UserAccount).WithMany(p => p.UserTransactions).HasForeignKey(p => p.UserAccountId);
            this.HasOptional<Account>(p => p.RecipientAccount).WithMany(p => p.RecipientTransactions).HasForeignKey(p => p.RecipientAccountId);
        }
    }
}
