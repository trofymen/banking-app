﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankingApp.EF.Entities
{
    public class Account
    {
        public Account()
        {
            this.UserTransactions = new List<Transaction>();
            this.RecipientTransactions = new List<Transaction>();
        }
        public int AccountId { get; set; }
        public long UserId { get; set; }
        [DefaultValue(0)]
        public double Balance { get; set; }
        public byte[] RowVersion { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Transaction> UserTransactions { get; set; }
        public virtual ICollection<Transaction> RecipientTransactions { get; set; }
    }
}
