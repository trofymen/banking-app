﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using EF.Entities;
using EF.Entities.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BankingApp.EF.Entities
{
    public class User : IdentityUser<long, UserLogin, UserRole, UserClaim>
    {
        public User()
        {
            this.Accounts = new List<Account>();
        }
        public DateTime CreateDate { get; set; }

        public virtual ICollection<Account> Accounts { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, long> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            return userIdentity;
        }

    }
}
