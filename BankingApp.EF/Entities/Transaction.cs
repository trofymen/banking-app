﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.EF.Enums;

namespace BankingApp.EF.Entities
{
    public class Transaction
    {
        public int TransactionId { get; set; }
        public double Amount { get; set; }
        public DateTime Date { get; set; }
        public TransactionType TransactionType { get; set; }
        public int UserAccountId { get; set; }
        public int? RecipientAccountId { get; set; }

        public virtual Account UserAccount { get; set; }
        public virtual Account RecipientAccount { get; set; }

    }
}
