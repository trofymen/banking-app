﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using BankingApp.EF.Entities;

namespace BankingApp.EF.Context
{
    public interface IBankingAppContext: IDisposable
    {
        IDbSet<Account> Accounts { get; set; }
        IDbSet<Transaction> Transactions { get; set; }
    }
}
