﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using BankingApp.EF.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BankingApp.EF.Context
{
    public class BankingAppDBInitializer : CreateDatabaseIfNotExists<BankingAppContext>
    {
        protected override void Seed(BankingAppContext context)
        {
            //var manager = new UserManager<User>(new UserStore<User>(new BankingAppContext()));

            //var user = new User()
            //{
            //    UserName = "SuperPowerUser",
            //    Email = "taiseer.joudeh@mymail.com",
            //    EmailConfirmed = true
            //};

            //manager.Create(user, "123456");

            IList<User> users = new List<User>()
            {
                new User { UserName = "Nick", PasswordHash = "123" },
                new User { UserName = "John", PasswordHash = "456" },
                new User { UserName = "Michail", PasswordHash = "789" }
            };
            foreach (User user in users)
                context.Users.Add(user);
            context.SaveChanges();

            IList<Account> accounts = new List<Account>();
            accounts.Add(new Account() { User = users[0], Balance = 100 });
            accounts.Add(new Account() { User = users[1], Balance = 200 });
            accounts.Add(new Account() { User = users[2], Balance = 300 });

            foreach (Account account in accounts)
                context.Accounts.Add(account);
            context.SaveChanges();

            //IList<Operation> operations = new List<Operation>()
            //{
            //    new Operation { OperationName = "Deposite" },
            //    new Operation { OperationName = "Withdraw" },
            //    new Operation { OperationName = "Transfer" }
            //};
            //foreach (Operation operation in operations)
            //    context.Operations.Add(operation);
            //context.SaveChanges();

            base.Seed(context);
        }
    }
}
