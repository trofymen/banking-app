﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using BankingApp.EF.EntitiesConfigurations;
using System.Configuration;
using BankingApp.EF.Entities;
using System.Diagnostics;
using Microsoft.AspNet.Identity.EntityFramework;
using EF.Entities;
using EF.Entities.Identity;

namespace BankingApp.EF.Context
{
    public class BankingAppContext : IdentityDbContext<User, Role, long, UserLogin, UserRole, UserClaim>, IBankingAppContext
    {
        private static string _connectionString = "name=myConnectionString";
        private static int _count = 0;
        public BankingAppContext() : base(_connectionString)
        {
            _count++;
            Debug.WriteLine("context = {0}",_count);
            //Database.SetInitializer<BankingAppContext>(new BankingAppDBInitializer());
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<BankingAppContext, BankingApp.EF.Migrations.Configuration>(_connectionString));
        }
        public static BankingAppContext Create()
        {
            return new BankingAppContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<UserClaim>().ToTable("UserClaims");
            modelBuilder.Entity<UserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<UserRole>().ToTable("UserRoles");
            modelBuilder.Entity<Role>().ToTable("Roles");

            modelBuilder.Configurations.Add(new TransactionConfiguration());
            modelBuilder.Configurations.Add(new AccountConfiguration());
        }
        public IDbSet<Account> Accounts { get; set; }
        public IDbSet<Transaction> Transactions { get; set; }
    }
}
