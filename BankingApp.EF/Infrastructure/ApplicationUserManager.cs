﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.EF.Context;
using BankingApp.EF.Infrastructure;
using EF.Entities;
using EF.Entities.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace BankingApp.EF.Entities
{
    public class ApplicationUserManager: UserManager<User, long>
    {
        public ApplicationUserManager(IUserStore<User, long> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            BankingAppContext appDbContext = context.Get<BankingAppContext>();
            ApplicationUserManager userManager = new ApplicationUserManager(new ApplicationUserStore(appDbContext));

            userManager.UserValidator = new UserValidator<User, long>(userManager)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = false,
            };

            userManager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false
            };
            return userManager;
        }
    }
}
