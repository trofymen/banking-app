﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.EF.Context;
using BankingApp.EF.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace BankingApp.EF.Infrastructure
{
    public class ApplicationRoleManager : RoleManager<Role, long>
    {
        public ApplicationRoleManager(IRoleStore<Role, long> roleStore)
            : base(roleStore)
        {

        }
        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var store = new ApplicationRoleStore(context.Get<BankingAppContext>());

            var appRoleManager = new ApplicationRoleManager(store);

            return appRoleManager;
        }
    }
}
