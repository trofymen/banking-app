﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.EF.Context;
using BankingApp.EF.Entities;
using EF.Entities.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BankingApp.EF.Infrastructure
{
    public class ApplicationUserStore : UserStore<User, Role, long, UserLogin, UserRole, UserClaim>
    {
        public ApplicationUserStore()
            : base(new IdentityDbContext())
        {
            DisposeContext = true;
        }

        public ApplicationUserStore(BankingAppContext context)
            : base(context)
        {

        }
    }
}
