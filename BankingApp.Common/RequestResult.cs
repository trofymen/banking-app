﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp.Common
{
    public class RequestResult
    {
        public RequestResult()
        {
            Status = RequestResultStatusEnum.Success;
        }
        public string Message { get; set; }
        public RequestResultStatusEnum Status { get; set; }
    }

    public class RequestResultData<T> : RequestResult
    {
        public T Data { get; set; }
    }
}
