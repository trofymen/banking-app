﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace BankingApp.Common
{
    public sealed class IoC
    {
        private static IUnityContainer _container = new UnityContainer();
        public static IUnityContainer Container
        {
            get { return _container; }
            set { _container = value; }
        }
    }
}
