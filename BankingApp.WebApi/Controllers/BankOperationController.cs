﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using BankingApp.Common;
using BankingApp.Core.Interfaces;
using BankingApp.Core.Services;
using BankingApp.ViewModel.Models;
using BankingApp.WebApi.AntiForgeryValidators;
using Microsoft.Practices.Unity;
using SendGrid.CSharp.HTTP.Client;

namespace BankingApp.WebApi.Controllers
{
    [Authorize(Roles = "Admin,Client")]
    [RoutePrefix("api")]
    //[ValidateAntiForgeryTokenFilter]
    public class BankOperationController : BaseApiController
    {
        private readonly IBankOperationService bankOperationService;

        public BankOperationController(IBankOperationService bankOperationService)
        {
            this.bankOperationService = bankOperationService;
        }

        [Route("clientinfo")]
        public async Task<IHttpActionResult> GetUserInfoAsync()
        {
            //var str = AntiForgery.GetHtml().ToString();

            RequestResult requestResult = await bankOperationService.GetUserInfoById(UserId);

            if (requestResult.Status == RequestResultStatusEnum.Error)
                return BadRequest(requestResult.Message);
            
            return Ok(requestResult);
        }

        [ValidateAntiForgeryTokenFilter]
        [Route("operation/deposit")]
        public async Task<IHttpActionResult> DepositeMoney([FromBody] double moneyToDeposit)
        {
            string cookieToken;
            string formToken;
            AntiForgery.GetTokens("", out cookieToken, out formToken);
            RequestResult requestResult = await this.bankOperationService.DepositeMoneyAsync(UserId, moneyToDeposit);

            if (requestResult.Status == RequestResultStatusEnum.Error)
                return BadRequest(requestResult.Message);

            return Ok(requestResult);
        }

        [ValidateAntiForgeryTokenFilter]
        [Route("operation/withdraw")]
        public async Task<IHttpActionResult> WithDrawMoney([FromBody] double moneyToWithDraw)
        {
            RequestResult requestResult = await this.bankOperationService.WithDrawMoneyAsync(UserId, moneyToWithDraw);

            if (requestResult.Status == RequestResultStatusEnum.Error)
                return BadRequest(requestResult.Message);

            return Ok(requestResult);
        }

        [ValidateAntiForgeryTokenFilter]
        [Route("operation/transfer")]
        public async Task<IHttpActionResult> TransferMoney(TransferViewModel transferInfo)
        {
            RequestResult requestResult = await this.bankOperationService.TransferMoneyAsync(UserId, transferInfo);

            if (requestResult.Status == RequestResultStatusEnum.Error)
                return BadRequest(requestResult.Message);

            return Ok(requestResult);
        }

        [Route("operation/transactions")]
        public async Task<IHttpActionResult> GetAllTransactions()
        {
            RequestResult requestResult = await this.bankOperationService.GetAllUserTransactionsAsync(UserId);

            if (requestResult.Status == RequestResultStatusEnum.Error)
                return BadRequest(requestResult.Message);

            return Ok(requestResult);
        }

        [Route("operation/allaccounts")]
        public async Task<IHttpActionResult> GetAllAccountsForTransfer()
        {
            RequestResult requestResult = await this.bankOperationService.GetAllAccountsNumbersAsync(UserId);

            if (requestResult.Status == RequestResultStatusEnum.Error)
                return BadRequest(requestResult.Message);

            return Ok(requestResult);
        }
    }
}
