﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.SessionState;
using BankingApp.ViewModel.Models;
using Newtonsoft.Json;

namespace BankingApp.WebApi.Controllers
{
    [Authorize(Roles = "Admin,Client")]
    [RoutePrefix("api")]
    public class AntiForgeryController: BaseApiController
    {
        public static ConcurrentDictionary<string, Dictionary<string, string>> formTokens = new ConcurrentDictionary<string, Dictionary<string, string>>();

        [Route("antiforgerytoken")]
        public HttpResponseMessage Get(string formId = null)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

            var userName = User.Identity.Name;

            AntiForgeryTokenModel antiForgeryTokenModel = new AntiForgeryTokenModel()
            {
                formId = string.IsNullOrEmpty(formId) ? Guid.NewGuid().ToString() : formId,
                antiForgeryToken = EncriptSha256(userName)
            };

            if (formTokens.ContainsKey((userName)))
            {
                if (formTokens[userName].ContainsKey((antiForgeryTokenModel.formId)))
                {
                    formTokens[userName][antiForgeryTokenModel.formId] = antiForgeryTokenModel.antiForgeryToken;
                }
                else
                    formTokens[userName].Add(antiForgeryTokenModel.formId, antiForgeryTokenModel.antiForgeryToken);
            }
            else
            {
                Dictionary<string, string> antiForgeryTokenInfo = new Dictionary<string, string>();
                antiForgeryTokenInfo.Add(antiForgeryTokenModel.formId, antiForgeryTokenModel.antiForgeryToken);
                formTokens.GetOrAdd(userName, antiForgeryTokenInfo);
            }
           
            response.Content = new StringContent(JsonConvert.SerializeObject(antiForgeryTokenModel), Encoding.UTF8, "application/json");

            return response;
        }

        [NonAction]
        private static string EncriptSha256(string userName)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            string expression = userName + DateTime.Now + Guid.NewGuid();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(expression), 0, Encoding.UTF8.GetByteCount(expression));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}