﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BankingApp.Common;
using BankingApp.Core.Interfaces;
using BankingApp.EF.Entities;
using BankingApp.EF.Enums;
using BankingApp.ViewModel.Models;
using Microsoft.AspNet.Identity;

namespace BankingApp.WebApi.Controllers
{
    [RoutePrefix("api/account")]
    public class AccountsController : BaseApiController
    {
        private readonly IBankOperationService userService;
        public AccountsController(IBankOperationService userService)
        {
            this.userService = userService;
        }

        [Authorize(Roles = "Admin")]
        [Route("user/{id:long}", Name = "GetUserById")]
        public async Task<IHttpActionResult> GetUser(long Id)
        {
            var user = await this.AppUserManager.FindByIdAsync(Id);

            if (user != null)
            {
                return Ok();
            }
            return NotFound();
        }

        [AllowAnonymous]
        [Route("register")]
        public async Task<IHttpActionResult> CreateUser(CreateUserViewModel createUserModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new User()
            {
                UserName = createUserModel.Username,
                Email = "anonim@anonim.com",
                EmailConfirmed = true,
                CreateDate = DateTime.Now
            };

            IdentityResult addUserResult = await this.AppUserManager.CreateAsync(user, createUserModel.Password);

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }

            IdentityResult addRoleResult = await this.AppUserManager.AddToRoleAsync(user.Id, RoleType.Client.ToString());

            if (!addRoleResult.Succeeded)
            {
                return GetErrorResult(addRoleResult);
            }

            RequestResult requestResult = await this.userService.CreateAccountForUser(user);

            Uri locationHeader = new Uri(Url.Link("GetUserById", new { id = user.Id }));

            if (requestResult.Status == RequestResultStatusEnum.Error)
            {
                IdentityResult createAccountResult = new IdentityResult(requestResult.Message);
                return GetErrorResult(createAccountResult);
            }
            return Created(locationHeader, requestResult);
        }
    }
}
