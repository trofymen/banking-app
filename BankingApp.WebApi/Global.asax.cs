﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using System.Web.SessionState;
using BankingApp.EF.Context;
using BankingApp.EF.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BankingApp.WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            new UnityConfig().RegisterComponents();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            

            //Dictionary<string, HttpSessionState> sessionData =
            //           new Dictionary<string, HttpSessionState>();
            //Application["s"] = sessionData;
        }

        //public override void Init()
        //{
        //    this.PostAuthenticateRequest += WebApiApplication_PostAuthenticateRequest;
        //    base.Init();
        //}

        //void WebApiApplication_PostAuthenticateRequest(object sender, EventArgs e)
        //{
        //    System.Web.HttpContext.Current.SetSessionStateBehavior(
        //        SessionStateBehavior.Required);
        //}


        //protected void Session_Start(object sender, EventArgs e)
        //{
        //    Dictionary<string, HttpSessionState> sessionData =
        //      (Dictionary<string, HttpSessionState>)Application["s"];

        //    if (sessionData.Keys.Contains(HttpContext.Current.Session.SessionID))
        //    {
        //        sessionData.Remove(HttpContext.Current.Session.SessionID);
        //        sessionData.Add(HttpContext.Current.Session.SessionID,
        //                        HttpContext.Current.Session);
        //    }
        //    else
        //    {
        //        sessionData.Add(HttpContext.Current.Session.SessionID,
        //                        HttpContext.Current.Session);
        //    }
        //    Application["s"] = sessionData;
        //}
        //protected void Session_End(object sender, EventArgs e)
        //{
        //    Dictionary<string, HttpSessionState> sessionData =
        //       (Dictionary<string, HttpSessionState>)Application["s"];
        //    sessionData.Remove(HttpContext.Current.Session.SessionID);
        //    Application["s"] = sessionData;
        //}
    }
}
