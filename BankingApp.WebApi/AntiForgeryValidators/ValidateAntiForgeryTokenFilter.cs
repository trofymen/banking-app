﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Helpers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.SessionState;
using BankingApp.ViewModel.Models;
using BankingApp.WebApi.Controllers;

namespace BankingApp.WebApi.AntiForgeryValidators
{
    public sealed class ValidateAntiForgeryTokenFilter : ActionFilterAttribute
    {
        private const string XsrfHeader = "XSRF-TOKEN";

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            HttpRequestHeaders headers = actionContext.Request.Headers;
            IEnumerable<string> xsrfTokenList;

            if (!headers.TryGetValues(XsrfHeader, out xsrfTokenList))
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                return;
            }

            var userName = actionContext.RequestContext.Principal.Identity.IsAuthenticated ? actionContext.RequestContext.Principal.Identity.Name : null;
            if (userName == null)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                return;
            }

            string tokenHeaderValue = xsrfTokenList.First();
            var seperateData = tokenHeaderValue.Split('!');

            AntiForgeryTokenModel antiForgeryTokenModel = new AntiForgeryTokenModel()
            {
                formId = seperateData[0],
                antiForgeryToken = seperateData[1]
            };

            try
            {
                if (!ValidateAntiForgeryToken(userName, antiForgeryTokenModel))
                {
                    throw new HttpAntiForgeryException();
                }
            }
            catch (HttpAntiForgeryException)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            }
            base.OnActionExecuting(actionContext);
        }

        private bool ValidateAntiForgeryToken(string userName, AntiForgeryTokenModel antiForgeryTokenModel)
        {
            if (AntiForgeryController.formTokens.ContainsKey(userName))
            {
                var valuesByUserName = AntiForgeryController.formTokens[userName];
                if (valuesByUserName.ContainsKey(antiForgeryTokenModel.formId))
                {
                    return valuesByUserName[antiForgeryTokenModel.formId].Contains(antiForgeryTokenModel.antiForgeryToken);
                }
            }
            return false;
        }
    }
}