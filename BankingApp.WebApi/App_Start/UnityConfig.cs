using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;
using BankingApp.DAL.UnitOfWork;
using BankingApp.DAL.Repositories;
using BankingApp.DAL.Facade.Interfaces;
using BankingApp.DAL.Facade;
using BankingApp.Core.Interfaces;
using BankingApp.Core.Services;
using BankingApp.Common;
using BankingApp.EF.Context;
using System.Data.Entity;
using BankingApp.EF.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BankingApp.WebApi
{
    public class UnityConfig
    {
        private IUnityContainer _container;
        
        public void RegisterComponents()
        {
            _container = IoC.Container;

            _container.RegisterType<IUnitOfWork, UnitOfWork>();

            _container.RegisterType<IRepositoryStorage, RepositoryStorage>();
            _container.RegisterType(typeof(IRepository<>), typeof(Repository<>));

            //_container.RegisterType<IdentityDbContext<User>, BankingAppContext>();    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            _container.RegisterType<IBankOperationFacade, BankOperationFacade>();
            _container.RegisterType<ITransactionFacade, TransactionFacade>();
            _container.RegisterType<IAccountFacade, AccountFacade>();

            _container.RegisterType<IBankOperationService, BankOperationService>();
            _container.RegisterType<ITransactionService, TransactionService>();
            _container.RegisterType<IAccountService, AccountService>();

            
            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(_container);
        }
    }
}