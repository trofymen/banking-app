﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.Common;
using BankingApp.Core.Interfaces;
using BankingApp.DAL.UnitOfWork;
using BankingApp.ViewModel.Models;
using Microsoft.Practices.Unity;

namespace BankingApp.Core.Services
{
    public class AccountService : IAccountService
    {
        public async Task<BankAccountViewModel> GetUserAccountByUserId(long userId)
        {
            using (var uow = IoC.Container.Resolve<IUnitOfWork>())
            {
                return await uow.AccountFacade.GetAccountByUserIdAsync(userId);
            }
        }
    }
}
