﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.DAL.UnitOfWork;
using BankingApp.ViewModel.Models;
using BankingApp.Core.Interfaces;
using Microsoft.Practices.Unity;
using BankingApp.Common;
using BankingApp.EF.Entities;
using BankingApp.EF.Enums;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Diagnostics;

namespace BankingApp.Core.Services
{
    public class BankOperationService: IBankOperationService
    {
        private async Task<BankAccountViewModel> GetAccountInfo(IUnitOfWork uow, long userId)
        {
            var accountInfo = await uow.AccountFacade.GetAccountByUserIdAsync(userId);
            return accountInfo;
        }

        public async Task<RequestResult> GetUserInfoById(long userId)
        {
            RequestResultData<UserInfoViewModel> requestResult = new RequestResultData<UserInfoViewModel>();
            try
            {
                using (var uow = IoC.Container.Resolve<IUnitOfWork>())
                {
                    var userInfo = await uow.UserFacade.GetUserInfoByIdAsync(userId);
                    userInfo.Account = await uow.AccountFacade.GetAccountByUserIdAsync(userId);
                    requestResult.Data = userInfo;
                }
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex.Message);
            }
            return requestResult;
        }

        public async Task<RequestResult> CreateAccountForUser(User userId)
        {
            RequestResult requestResult = new RequestResult();
            try
            {
                using (var uow = IoC.Container.Resolve<IUnitOfWork>())
                {
                    uow.AccountFacade.CreateAccount(userId);
                    await uow.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex.Message);
            }
            return requestResult;
        }

        public async Task<RequestResult> DepositeMoneyAsync(long userId, double moneyToDeposite)
        {
            //BankAccountViewModel userInfo;
            TransactionType typeTransaction = TransactionType.Deposit;
            RequestResultData<double> requestResult = new RequestResultData<double>();
            bool saveFailed = false;
            using (var uow = IoC.Container.Resolve<IUnitOfWork>())
            {
                try
                {
                    var userInfo = await GetAccountInfo(uow, userId);
                    double balance = userInfo.Balance;
                    if (moneyToDeposite <= 0)
                    {
                        requestResult.Message = "amount must be positive!";
                        requestResult.Status = RequestResultStatusEnum.WrongData;
                        requestResult.Data = balance;
                    }
                    else
                    {
                        double newBalance = balance + moneyToDeposite;
                        uow.AccountFacade.ChangeBalanceByUserId(userId, newBalance);
                        uow.TransactionFacade.AddTransactionAsync(userInfo.AccountId, null, moneyToDeposite, typeTransaction);
                        await uow.SaveChangesAsync();
                        requestResult.Data = newBalance;
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    saveFailed = true;
                }
                catch (Exception ex)
                {
                    return GetErrorResult(ex.Message);
                }
            }
            if(saveFailed)
            {
                RequestResult newRequestResult = await DepositeMoneyAsync(userId, moneyToDeposite);
                requestResult = newRequestResult as RequestResultData<double>;
            }
            return requestResult;
        }

        public async Task<RequestResult> WithDrawMoneyAsync(long userId, double moneyToWithDraw)
        {
            //BankAccountViewModel userInfo;
            TransactionType typeTransaction = TransactionType.Withdraw;
            RequestResultData<double> requestResult = new RequestResultData<double>();
            bool saveFailed = false;
            using (var uow = IoC.Container.Resolve<IUnitOfWork>())
            {
                try
                {
                    var userInfo = await GetAccountInfo(uow, userId);
                    double balance = userInfo.Balance;
                    if (moneyToWithDraw <= 0)
                    {
                        requestResult.Message = "amount must be positive!";
                        requestResult.Status = RequestResultStatusEnum.WrongData;
                        requestResult.Data = balance;
                    }
                    else if (balance < moneyToWithDraw)
                    {
                        requestResult.Message = "not enough money!";
                        requestResult.Status = RequestResultStatusEnum.WrongData;
                        requestResult.Data = balance;
                    }
                    else
                    {
                        double newBalance = balance - moneyToWithDraw;
                        uow.AccountFacade.ChangeBalanceByUserId(userId, newBalance);
                        uow.TransactionFacade.AddTransactionAsync(userInfo.AccountId, null, moneyToWithDraw, typeTransaction);
                        await uow.SaveChangesAsync();
                        requestResult.Data = newBalance;
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    saveFailed = true;
                }
                catch (Exception ex)
                {
                    return GetErrorResult(ex.Message);
                }
            }
            if (saveFailed)
            {
                RequestResult newRequestResult = await WithDrawMoneyAsync(userId, moneyToWithDraw);
                requestResult = newRequestResult as RequestResultData<double>;
            }
            return requestResult;
        }

        public async Task<RequestResult> TransferMoneyAsync(long userSenderId, TransferViewModel transferInfo)
        {
            //BankAccountViewModel userSenderInfo;
            //BankAccountViewModel recipientAccountInfo;
            TransactionType typeTransaction = TransactionType.Transfer;
            RequestResultData<double> requestResult = new RequestResultData<double>();
            bool saveFailed = false;
            using (var uow = IoC.Container.Resolve<IUnitOfWork>())
            {
                try
                {
                    var userSenderInfo = await GetAccountInfo(uow, userSenderId);
                    if (userSenderInfo.Balance < transferInfo.MoneyToTransfer)
                    {
                        requestResult.Message = "sender has not enough money!";
                        requestResult.Status = RequestResultStatusEnum.WrongData;
                        requestResult.Data = userSenderInfo.Balance;
                        return requestResult;
                    }

                    var recipientAccountInfo = await uow.AccountFacade.GetAccountByIdAsync(transferInfo.RecipientAccount);

                    if (recipientAccountInfo == null)
                    {
                        requestResult.Message = "recipient account " + transferInfo.RecipientAccount + " not found!";
                        requestResult.Status = RequestResultStatusEnum.WrongData;
                    }
                    else
                    {
                        double newSenderBalance = userSenderInfo.Balance - transferInfo.MoneyToTransfer;
                        double newRecipientBalance = recipientAccountInfo.Balance + transferInfo.MoneyToTransfer;

                        uow.AccountFacade.ChangeBalanceByUserId(userSenderId, newSenderBalance);
                        uow.AccountFacade.ChangeBalanceByAccountId(recipientAccountInfo.AccountId, newRecipientBalance);
                        uow.TransactionFacade.AddTransactionAsync(userSenderInfo.AccountId, recipientAccountInfo.AccountId, transferInfo.MoneyToTransfer, typeTransaction);

                        await uow.SaveChangesAsync();
                        requestResult.Data = newSenderBalance;
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    saveFailed = true;
                }
                catch (Exception ex)
                {
                    return GetErrorResult(ex.Message);
                }
            }
            if (saveFailed)
            {
                RequestResult newRequestResult = await TransferMoneyAsync(userSenderId, transferInfo);
                requestResult = newRequestResult as RequestResultData<double>;
            }
            return requestResult;
        }

        public async Task<RequestResult> GetAllUserTransactionsAsync(long userId)
        {
            BankAccountViewModel userInfo;
            //List<TransactionViewModel> transactions;
            RequestResultData<List<TransactionViewModel>> requestResult = new RequestResultData<List<TransactionViewModel>>();
            try
            {
                using (var uow = IoC.Container.Resolve<IUnitOfWork>())
                {
                    userInfo = await GetAccountInfo(uow, userId);
                    int userAccount = userInfo.AccountId;
                    var transactions = await uow.TransactionFacade.GetAllTransactionsListAsync(userAccount);
                    if (transactions.Count == 0)
                    {
                        requestResult.Message = "your account does not have any transaction yet!";
                        requestResult.Status = RequestResultStatusEnum.WrongData;
                    }
                    else
                    {
                        requestResult.Data = transactions;
                    }
                }
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex.Message);
            }
            return requestResult;
        }

        public async Task<RequestResult> GetAllAccountsNumbersAsync(long userId)
        {
            RequestResultData<List<int>> requestResult = new RequestResultData<List<int>>();
            try
            {
                using (var uow = IoC.Container.Resolve<IUnitOfWork>())
                {
                    var accountsNumbers = await uow.AccountFacade.GetAllAccounts(userId);
                    if (accountsNumbers.Count == 0)
                    {
                        requestResult.Message = "none";
                        requestResult.Status = RequestResultStatusEnum.WrongData;
                    }
                    else
                    {
                        requestResult.Data = accountsNumbers;
                    }
                }
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex.Message);
            }
            return requestResult;
        }

        private RequestResult GetErrorResult(string exceptionMessage)
        {
            return new RequestResult { Status = RequestResultStatusEnum.Error, Message = exceptionMessage };
        }        
    }
}