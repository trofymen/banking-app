﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.DAL.UnitOfWork;
using BankingApp.ViewModel.Models;
using BankingApp.Core.Interfaces;
using Microsoft.Practices.Unity;
using BankingApp.Common;

namespace BankingApp.Core.Services
{
    public class TransactionService: ITransactionService
    {
        public async Task<List<TransactionViewModel>> GetAllTransactions(int accountId)
        {
            using (var uow = IoC.Container.Resolve<IUnitOfWork>())
            {
                return await uow.TransactionFacade.GetAllTransactionsListAsync(accountId);
            }
        }
    }
}
