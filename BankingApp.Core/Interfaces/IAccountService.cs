﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.ViewModel.Models;

namespace BankingApp.Core.Interfaces
{
    public interface IAccountService
    {
        Task<BankAccountViewModel> GetUserAccountByUserId(long userId);
    }
}
