﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.Common;
using BankingApp.EF.Entities;
using BankingApp.ViewModel.Models;

namespace BankingApp.Core.Interfaces
{
    public interface IBankOperationService
    {
        Task<RequestResult> GetUserInfoById(long userId);
        Task<RequestResult> CreateAccountForUser(User userId);
        Task<RequestResult> DepositeMoneyAsync(long userId, double moneyToDeposite);
        Task<RequestResult> WithDrawMoneyAsync(long userId, double moneyToWithDraw);
        Task<RequestResult> TransferMoneyAsync(long userSenderId, TransferViewModel transferInfo);
        Task<RequestResult> GetAllUserTransactionsAsync(long userId);
        Task<RequestResult> GetAllAccountsNumbersAsync(long userId);
    }
}
