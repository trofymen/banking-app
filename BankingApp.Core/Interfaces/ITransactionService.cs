﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.ViewModel.Models;

namespace BankingApp.Core.Interfaces
{
    public interface ITransactionService
    {
        Task<List<TransactionViewModel>> GetAllTransactions(int accountId);
    }
}
