﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.EF.Enums;
using BankingApp.ViewModel.Models;

namespace BankingApp.DAL.Facade.Interfaces
{
    public interface ITransactionFacade
    {
        Task<List<TransactionViewModel>> GetAllTransactionsListAsync(int accountId);
        void AddTransactionAsync(int userAccountId, int? recipientAccountId, double amount, TransactionType type);
    }
}
