﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.EF.Entities;
using BankingApp.ViewModel.Models;

namespace BankingApp.DAL.Facade.Interfaces
{
    public interface IAccountFacade
    {
        Task<BankAccountViewModel> GetAccountByUserIdAsync(long userId);
        void CreateAccount(User userId);
        void ChangeBalanceByUserId(long userId, double newBalance);
        Task<BankAccountViewModel> GetAccountByIdAsync(int accountId);
        void ChangeBalanceByAccountId(int accountId, double newBalance);
        Task<List<int>> GetAllAccounts(long userId);
    }
}
