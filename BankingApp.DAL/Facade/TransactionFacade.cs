﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.DAL.Facade.Interfaces;
using BankingApp.DAL.Repositories;
using BankingApp.EF.Entities;
using BankingApp.ViewModel.Models;
using BankingApp.EF.Enums;

namespace BankingApp.DAL.Facade
{
    public class TransactionFacade: BaseFacade, ITransactionFacade
    {
        public TransactionFacade(IRepositoryStorage storage) : base(storage)
        {
        }

        public async Task<List<TransactionViewModel>> GetAllTransactionsListAsync(int accountId)
        {
            var transactions = await GetRepository<Transaction>().Query().Where(item => item.UserAccountId == accountId || item.RecipientAccountId == accountId).ToListAsync();
            var transactionsInfo = transactions.Select(item => new TransactionViewModel
            {
                TransactionId = item.TransactionId,
                Amount = item.Amount,
                Date = item.Date,
                TransactionType = item.TransactionType.ToString(),
                UserAccountId = item.UserAccountId,
                RecipientAccountId = item.RecipientAccountId
            }).ToList();
            return transactionsInfo;
            
        }

        public void AddTransactionAsync(int userAccountId, int? recipientAccountId, double amount, TransactionType type)
        {
            if (type == TransactionType.Deposit || type == TransactionType.Withdraw)
            {
                GetRepository<Transaction>().Insert(new Transaction
                {
                    Amount = amount,
                    Date = DateTime.Now,
                    UserAccountId = userAccountId,
                    TransactionType = type
                });
            }
            else
            {
                GetRepository<Transaction>().Insert(new Transaction
                {
                    Amount = amount,
                    Date = DateTime.Now,
                    UserAccountId = userAccountId,
                    RecipientAccountId = recipientAccountId,
                    TransactionType = type
                });
            }
        }
    }
}
