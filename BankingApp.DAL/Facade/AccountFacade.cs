﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.DAL.Facade.Interfaces;
using BankingApp.DAL.Repositories;
using BankingApp.EF.Entities;
using BankingApp.ViewModel.Models;

namespace BankingApp.DAL.Facade
{
    public class AccountFacade: BaseFacade, IAccountFacade
    {
        public AccountFacade(IRepositoryStorage storage) : base(storage)
        {
        }
        //public async Task<BankAccountViewModel> GetAccountByUserIdAsync2(long userId)
        //{
        //    var account = await GetRepository<Account>().Query().FirstOrDefaultAsync(item => item.UserId == userId);
        //    if (account != null)
        //    {
        //        BankAccountViewModel accountInfo = new BankAccountViewModel
        //        {
        //            AccountId = account.AccountId,
        //            Balance = account.Balance
        //        };
        //        return accountInfo;
        //    }
        //    return null;
        //}

        public async Task<BankAccountViewModel> GetAccountByUserIdAsync(long userId)
        {
            var accountInfo = await GetRepository<Account>().Query().Where(item => item.UserId == userId).Select(item => new BankAccountViewModel
            {
                AccountId = item.AccountId,
                Balance = item.Balance
            }).FirstOrDefaultAsync();

            return accountInfo;
        }

        //public async Task<BankAccountViewModel> GetAccountByIdAsync2(int accountId)
        //{
        //    var account = await GetRepository<Account>().Query().FirstOrDefaultAsync(item => item.AccountId == accountId);
        //    if (account != null)
        //    {
        //        BankAccountViewModel accountInfo = new BankAccountViewModel
        //        {
        //            AccountId = account.AccountId,
        //            Balance = account.Balance
        //        };
        //        return accountInfo;
        //    }
        //    return null;
        //}

        public async Task<BankAccountViewModel> GetAccountByIdAsync(int accountId)
        {
            var accountInfo = await GetRepository<Account>().Query().Where(item => item.AccountId == accountId).Select(item => new BankAccountViewModel
            {
                AccountId = item.AccountId,
                Balance = item.Balance
            }).FirstOrDefaultAsync();

            return accountInfo;
        }

        public void CreateAccount(User userId)
        {
            GetRepository<Account>().Insert(new Account {
                UserId = userId.Id
            });
        }

        public void ChangeBalanceByUserId(long userId, double newBalance)
        {
            var userAccount = GetRepository<Account>().Query().FirstOrDefault(item => item.UserId == userId);
            userAccount.Balance = newBalance;
        }

        public void ChangeBalanceByAccountId(int accountId, double newBalance)
        {
            var userAccount = GetRepository<Account>().Query().FirstOrDefault(item => item.AccountId == accountId);
            userAccount.Balance = newBalance;
        }

        //public async Task<List<BankAccountViewModel>> GetAllAccounts2()
        //{
        //    var usersAccounts = await GetRepository<Account>().Query().ToListAsync();
        //    var accounts = usersAccounts.Select(item => new BankAccountViewModel
        //    {
        //        AccountId = item.AccountId
        //    }).ToList();
        //    return accounts;
        //}

        public async Task<List<int>> GetAllAccounts(long userId)
        {
            var accounts = await GetRepository<Account>().Query().Where(item => item.UserId != userId).Select(item => item.AccountId).ToListAsync();            
            return accounts;
        }
    }
}
