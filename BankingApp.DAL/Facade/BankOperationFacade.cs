﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.DAL.Facade.Interfaces;
using BankingApp.DAL.Repositories;
using BankingApp.EF.Entities;
using BankingApp.ViewModel.Models;

namespace BankingApp.DAL.Facade
{
    public class BankOperationFacade: BaseFacade, IBankOperationFacade
    {
        public BankOperationFacade(IRepositoryStorage storage) : base(storage)
        {
        }  
        public async Task<UserInfoViewModel> GetUserInfoByIdAsync(long userId)
        {
            var user = await GetRepository<User>().Query().FirstAsync(item => item.Id == userId);
            var userInfo = new UserInfoViewModel
            {
                UserName = user.UserName
            };
            return userInfo;
        }
    }
}
