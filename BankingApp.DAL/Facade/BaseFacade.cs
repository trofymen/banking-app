﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.DAL.Repositories;

namespace BankingApp.DAL.Facade
{
    public abstract class BaseFacade
    {
        protected readonly IRepositoryStorage storage;
        protected BaseFacade(IRepositoryStorage storage)
        {
            this.storage = storage;
        }
        protected IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            return this.storage.Repository<TEntity>();
        }
    }
}
