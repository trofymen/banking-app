﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp.DAL.Repositories
{
    public interface IRepository<TEntity> where TEntity: class
    {
        IQueryable<TEntity> Query();
        TEntity GetById(long id);
        void Insert(TEntity entity);
        void Delete(TEntity entity);
        void Update(TEntity entity);
    }
}
