﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.EF.Context;
using System.Data.Entity;

namespace BankingApp.DAL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        public readonly BankingAppContext _context;
        private IDbSet<TEntity> _entity;
        public string ErrorMessage { get; private set; }

        public Repository(BankingAppContext Context)
        {
            _context = Context;
            _entity = Context.Set<TEntity>();
        }

        public IQueryable<TEntity> Query()
        {
            return _entity.AsQueryable<TEntity>();
        }

        public TEntity GetById(long id)
        {
            return _entity.Find(id);
        }

        public void Insert(TEntity entity)
        {
            this._entity.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            this._entity.Remove(entity);
        }
        public virtual void Update(TEntity entity)
        {
            this._entity.Attach(entity);
            this._context.Entry(entity).State = EntityState.Modified;
        }        
    }
}
