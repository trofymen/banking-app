﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using BankingApp.EF.Context;
using BankingApp.Common;
using BankingApp.DAL.Repositories;
using System.Diagnostics;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BankingApp.DAL.Repositories
{
    public class RepositoryStorage : IRepositoryStorage
    {
        protected readonly BankingAppContext context;
        //protected readonly DbContext context;
        private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();
        private bool _disposed;
        private static int _count = 0;
        public RepositoryStorage()
        {
            //this.context = IoC.Container.Resolve<IdentityDbContext>();
            this.context = new BankingAppContext();
            _count++;
            Debug.WriteLine("repo storage = {0}",_count);
        }
        public IRepository<TEntity> Repository<TEntity>() where TEntity : class
        {
            if (this._repositories.Keys.Contains(typeof(TEntity)))
            {
                return this._repositories[typeof(TEntity)] as IRepository<TEntity>;
            }
            IRepository<TEntity> repository = new Repository<TEntity>(this.context);
            this._repositories.Add(typeof(TEntity), repository);
            return repository;
        }
        public Task<int> SaveChangesAsync()
        {
            return this.context.SaveChangesAsync();
        }

        public int SaveChanges()
        {
            return this.context.SaveChanges();
        }
        protected void Dispose(bool _disposing)
        {
            if (!_disposed)
            {
                if (_disposing)
                {
                    this.context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
