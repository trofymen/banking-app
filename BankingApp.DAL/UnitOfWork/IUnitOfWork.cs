﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.DAL.Facade.Interfaces;
using BankingApp.DAL.Repositories;

namespace BankingApp.DAL.UnitOfWork
{
    public interface IUnitOfWork: IDisposable
    {
        IBankOperationFacade UserFacade { get; }
        ITransactionFacade TransactionFacade { get; }
        IAccountFacade AccountFacade { get;}
        Task<int> SaveChangesAsync();
        int SaveChanges();
    }
}
