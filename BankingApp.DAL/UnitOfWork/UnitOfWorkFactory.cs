﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp.DAL.UnitOfWork
{
    public class UnitOfWorkFactory
    {
        private IUnitOfWork _uow;
        private static int _count = 0;
        public IUnitOfWork CreateUnitOfWork(IUnitOfWork uow)
        {
            _count++;
            Debug.WriteLine("uow_factory = {0}", _count);
            this._uow = uow;
            return this._uow;
        }
    }
}
