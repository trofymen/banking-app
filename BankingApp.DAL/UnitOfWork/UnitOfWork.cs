﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankingApp.Common;
using BankingApp.DAL.Facade;
using BankingApp.DAL.Facade.Interfaces;
using BankingApp.DAL.Repositories;
using Microsoft.Practices.Unity;
using BankingApp.EF.Context;

namespace BankingApp.DAL.UnitOfWork
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly IRepositoryStorage _storage;
        private IBankOperationFacade _userFacade;
        private ITransactionFacade _transactionFacade;
        private IAccountFacade _accountFacade;

        public UnitOfWork(IRepositoryStorage storage)
        {
            this._storage = storage;
        }
        public IBankOperationFacade UserFacade => this._userFacade ?? (this._userFacade = new BankOperationFacade(this._storage)); // new UserFacade(this._storage));
        public ITransactionFacade TransactionFacade => this._transactionFacade ?? (this._transactionFacade = new TransactionFacade(this._storage));
        public IAccountFacade AccountFacade => this._accountFacade ?? (this._accountFacade = new AccountFacade(this._storage)); //IoC.Container.Resolve<IAccountFacade>());

        public Task<int> SaveChangesAsync()
        {
            return this._storage.SaveChangesAsync();
        }

        public int SaveChanges()
        {
            return this._storage.SaveChanges();
        }

        public void Dispose()
        {
            this._storage.Dispose();
        }
    }
}
